<?php

namespace yagerguo\yii2post;

class PostModule extends \yii\base\Module
{
    public $controllerNamespace = 'yagerguo\yii2post\controllers\frontend';
    
    public $frontendViewPath;

    public function init()
    {
        parent::init();
    }
}
